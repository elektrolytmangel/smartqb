## SmartQb - a clock to look at
Die SmartQb ist eine funktionierende, neuartige, innovative  Wanduhr. 
Sie bietet verschiedene Funktionen wie z.B. einer neuartigen Zeitanzeige, 
einer Wetteranzeige und weiteren smarten Funktionen. 
Die nötigen Ressourcen um die Uhr selbst nachzubauen sind ind diesem Repo vorhanden.

Es müssen grundsätzlich folgende Schritte durchgeführt werden:
* 3-D Druckn des Gehäuses und Frontplatte (siehe [Konstruktion](https://gitlab.com/elektrolytmangel/smartqb/tree/master/Konstruktion))
* Verbinden der elektronischen Komponenten (siehe [Elektronik](https://gitlab.com/elektrolytmangel/smartqb/tree/master/Elektro))
* Aufsetzten des RasperryPi's und Installation der Software und Komponenten (siehe [RaspberryPi](https://gitlab.com/elektrolytmangel/smartqb/wikis/RaspberryPi-aufsetzen))
* Arduino Sketch auf Arduino Nano laden (siehe [Arduino](https://www.arduino.cc/en/Guide/HomePage))

Schon fertig, und die Uhr kann gestartet werden.

Eine Liste zu allen nötigen Komponenten, welche bestellt werden müssen, ist [hier](https://gitlab.com/elektrolytmangel/smartqb/blob/master/Elektro/St%C3%BCckliste_SmartQb.xlsx) zu finden.

![alt text](./smartqb.png?raw=true "smartQb")