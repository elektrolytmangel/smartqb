#include <ArduinoJson.h>

int ldrSensor = A0;   // Analog Eingang für 
int pirSensor = 2;    // Digitaler Eingang PIR Sensor
const int capacity = JSON_OBJECT_SIZE(2);
StaticJsonDocument<capacity> doc;

void setup() {
  Serial.begin(9600); 
  pinMode(pirSensor, INPUT);
}

void loop() {
  int pirState = digitalRead(pirSensor);
  int ldrValue = analogRead(ldrSensor);

  doc["ldr"] = ldrValue;
  doc["pir"] = pirState;
  /*
  create json:
  {
    "ldr": 0.6,
    "pir": 0
  }
  */
  serializeJson(doc, Serial);
  Serial.write("\r\n");
  delay(500); // ms
}
/***************************************************************************************
  -   Abgedunkelter Raum: 150 - 300
  -   Normaler Raum: 500 - 800
  -   Sonnenlicht: > 900
*/
