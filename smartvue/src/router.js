import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/profileSetup',
      name: 'profileSetup',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "profileSetup" */ './views/ProfileSetup.vue')
    },
    {
      path: '/wifiSetup',
      name: 'wifiSetup',
      component: () => import(/* webpackChunkName: "wifi" */ './views/WifiSetup.vue')
    },
    {
      path: '/location',
      name: 'location',
      component: () => import(/* webpackChunkName: "location" */ './views/Location.vue')
    },
    {
      path: '/off',
      name: 'off',
      component: () => import(/* webpackChunkName: "location" */ './views/Off.vue')
    },
    {
      path: '/changeLocation',
      name: 'changeLocation',
      component: () => import(/* webpackChunkName: "location" */ './views/ChangeLocation.vue')
    },
    {
      path: '/alarmClock',
      name: 'alarmClock',
      component: () => import(/* webpackChunkName: "location" */ './views/AlarmClock.vue')
    },
    {
      path: '/operationHours',
      name: 'operationHours',
      component: () => import(/* webpackChunkName: "location" */ './views/OperationHours.vue')
    },
    {
      path: '/about',
      name: 'about',
      component: () => import(/* webpackChunkName: "location" */ './views/About.vue')
    }
  ]
})
