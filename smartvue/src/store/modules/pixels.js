import axios from 'axios'

const state = {
  current_mode: 1,
  pixelTimer: null
}

const getters = {

}

const actions = {
  init ( { dispatch, state }) {
    state.pixelTimer = setInterval(function () {
      dispatch('get')}
      , 5000)
  },
  changeLedMode ({ commit, dispatch }, id) {
    // http put request to change led-mode
    axios.put(`${process.env.VUE_APP_API_ENDPOINT}/api/v1/pixels/${id}`)
    if (id !== 0) {
      commit('updateMode', id)
    }
    dispatch('l/updateWeather', 'dummy', { root: true })
  },
  get ({ commit }) {
    axios.get(`${process.env.VUE_APP_API_ENDPOINT}/api/v1/pixels/`).then((result) => {
      commit('updateMode', result.data.current_mode)
    }).catch(function (error) {
      console.log(error)
    })
  }
}

const mutations = {
  updateMode (state, id) {
    state.current_mode = id
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
