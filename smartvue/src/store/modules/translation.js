import moment from 'moment'

const state = {
  currentLang: {
  },
  languages: {
    de: {
      languageCode: 'de',
      title: 'SmartQb',
      welcome: 'Willkommen',
      createProfile: 'Bitte gib einen Profilnamen ein',
      profileName: 'Name',
      save: 'speichern',
      connInt: 'Internetverbindung herstellen',
      selectWifi: 'WLan-Netzwerk auswählen',
      refresh: 'aktualisieren',
      password: 'Passwort',
      connect: 'verbinden',
      wifiNotFound: 'Keine WLan-Netzwerke gefunden. Bitte aktiviere dein WLan.',
      conWithoutWifi: 'ohne Internetvebindung vortfahren',
      locService: 'Standort-Dienst einrichten',
      selLocInfo: 'Standort auf Karte auswählen',
      selPos: 'Ausgewählter Standort:',
      latLng: 'Länge & Breite',
      country: 'Land',
      timeZone: 'Zeitzone',
      curDateTime: 'Aktuelle Datums- und Zeitangabe',
      selLocation: 'Standort speichern',
      w3w: '3-Wort-Adresse',
      settings: 'Einstellungen',
      help: 'Hilfe',
      on: 'Ein',
      off: 'Aus',
      time: 'Zeit',
      dayNight: 'Tag / Nacht',
      weather: 'Wetter',
      selProfile: 'Profil auswählen',
      changeLoc: 'Standort anpassen',
      selLanguage: 'Sprache auswählen',
      done: 'auswählen',
      cancel: 'abbrechen',
      alarmTimer: 'Wecker einstellen',
      selectTime: 'Zeit auswählen',
      active: 'aktiviert',
      alarmStop: 'Wecker stoppen',
      operationHours: 'Betriebszeiten auswählen',
      uptimeOn: 'Einschalten um',
      uptimeOff: 'Ausschalten um',
      about: 'Über',
      home: 'Startseite',
      projectDesc: 'Das Projekt wurde erstellt durch',
      projectOf: 'Projekt an der HFTM',
      isFailed: 'Verbindung mit WLan ist fehlgeschlagen.',
      skip: 'überspringen'
    },
    en: {
      languageCode: 'en',
      title: 'SmartQb',
      welcome: 'Welcome',
      createProfile: 'Please choose a profilename',
      profileName: 'name',
      save: 'save',
      connInt: 'Setting up internet-connection',
      selectWifi: 'Select WiFi',
      refresh: 'refresh',
      password: 'password',
      connect: 'connect',
      wifiNotFound: 'No Wifis found. Please enable your wifi.',
      conWithoutWifi: 'continue without internet-connection',
      locService: 'Setting up location-services',
      selLocInfo: 'Select your location on map',
      selPos: 'You selected',
      latLng: 'Latitude & Longitude',
      country: 'Country',
      timeZone: 'Timezone',
      curDateTime: 'current date & time',
      selLocation: 'save location',
      w3w: 'What3WordAddress',
      settings: 'Settings',
      help: 'Help',
      on: 'On',
      off: 'Off',
      time: 'Time',
      dayNight: 'Day / Night',
      weather: 'Weather',
      selProfile: 'select profile',
      changeLoc: 'change location',
      selLanguage: 'select language',
      done: 'done',
      cancel: 'cancel',
      alarmTimer: 'set alarm-timer',
      selectTime: 'select time',
      active: 'activated',
      alarmStop: 'stop alarm-clock',
      operationHours: 'select uptime',
      uptimeOn: 'turn on at',
      uptimeOff: 'turn off at',
      about: 'About',
      home: 'Home',
      projectDesc: 'The project was created by',
      projectOf: 'A project at HFTM',
      isFailed: 'connection to wifi failed',
      skip: 'skip'
    }
  }
}

const getters = {
  currentLang (state) {
    return state.currentLang
  },
  languages (state) {
    return state.languages
  }
}

const actions = {
  setLanguage (context, lang) {
    moment.locale(lang.languageCode)
    context.commit('updateCurrentLang', lang)
    var location = context.rootGetters['l/currentLocation']
    context.dispatch('l/getWhat3Word', location, { root: true })
    context.dispatch('l/getWeather', location, { root: true })
  }
}

const mutations = {
  updateCurrentLang (state, lang) {
    state.currentLang = lang
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
