import axios from 'axios'

const state = {
  currentLocation: {
    lat: 47,
    lng: 7,
    country: '',
    city: '',
    time: '',
    timezone: '',
    gmtOffset: '',
    what3words: ''
  },
  currentWeather: {

  }
}

const getters = {
  currentLocation (state) {
    return state.currentLocation
  }
}

const actions = {
  async getLocationInfo ({ dispatch }, location) {
    await dispatch('getTimeZone', location)
    await dispatch('getWhat3Word', location)
    await dispatch('getWeather', location)
  },
  async getTimeZone ({ commit }, location) {
    let params = `?key=YMCOBAEP43PX&format=json&by=position&lat=${location.lat}&lng=${location.lng}`
    await axios.get(`http://api.timezonedb.com/v2.1/get-time-zone${params}`).then(async (result) => {
      let rawLoc = result.data
      if (rawLoc != null) {
        await commit('updateCurrentLocation', { rawLoc, location })
      }
    }).catch(function (error) {
      console.log(error)
    })
  },
  async getWhat3Word ({ commit, rootGetters }, location) {
    let lang = rootGetters['t/currentLang'].languageCode
    let params = `v3/convert-to-3wa?coordinates=${location.lat},${location.lng}&language=${lang}&key=LJIUKLUB`
    await axios.get(`https://api.what3words.com/${params}`).then(async (result) => {
      let w3w = result.data.words
      if (w3w != null) {
        await commit('updateWhat3Word', w3w)
      }
    }).catch(function (error) {
      console.log(error)
    })
  },
  async getWeather ({ dispatch, commit, rootGetters }, location) {
    var lang = rootGetters['t/currentLang'].languageCode
    var params = `/data/2.5/weather?lat=${location.lat}&lon=${location.lng}&units=metric&lang=${lang}&appid=f8e0e332a1ed514da432ea0c6b5312ae`
    // https://api.openweathermap.org/data/2.5/weather?q=London,uk&appid=YOUR_API_KEY
    await axios.get(`https://api.openweathermap.org/${params}`).then(async (result) => {
      var clouds = 0
      if (result.data.clouds) {
        clouds = result.data.clouds.all
      }
      var rain = 0
      if (result.data.rain) {
        if (result.data.rain['3h']) {
          rain = result.data.rain['3h'] // because the creator of openweatherapi are not going with js naming-convetions
        } else if (result.data.rain['1h']) {
          rain = result.data.rain['1h']
        }
      }
      let currentWeather = {
        city: result.data.name,
        weather: result.data.weather[0].description,
        icon: result.data.weather[0].icon,
        temp: result.data.main.temp.toFixed(1),
        sunrise: result.data.sys.sunrise,
        sunset: result.data.sys.sunset,
        country: result.data.sys.country,
        clouds: clouds,
        rain: rain
      }
      await commit('updateWeather', currentWeather)
    }).catch(function (error) {
      console.log(error)
    })
    dispatch('updateWeather', 'dummy')
  },
  updateWeather ({ state }, dummy) {
    // dummy is needed to call the method from another modul, do not know why, but it wont work
    axios.post(`${process.env.VUE_APP_API_ENDPOINT}/api/v1/weather/`, state.currentWeather)
      .catch(function (error) {
        console.log(error)
      })
  }
}

const mutations = {
  updateCurrentLocation (state, params) {
    let location = params.location
    let locationRaw = params.rawLoc
    state.currentLocation.lat = location.lat,
    state.currentLocation.lng = location.lng,
    state.currentLocation.country = locationRaw.countryName,
    state.currentLocation.time = locationRaw.formatted,
    state.currentLocation.gmtOffset = locationRaw.gmtOffset,
    state.currentLocation.timezone = locationRaw.zoneName
  },
  updateWhat3Word (state, w3w) {
    state.currentLocation.what3words = w3w
  },
  updateWeather (state, weather) {
    state.currentWeather = weather
    state.currentLocation.city = weather.city
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
