import axios from 'axios'

const state = {
  wifis: [],
  currentWifi: {
    ssid: '',
    encrypted: false,
    password: ''
  }
}

const getters = {
  currentWifi (state) {
    return state.currentWifi
  },
  password (state) {
    if (state.currentWifi == null) {
      return ''
    }
    return state.currentWifi.password
  }
}

const actions = {
  get ({ commit, rootState }) {
    axios.get(`${process.env.VUE_APP_API_ENDPOINT}/api/v1/wifis/`,
      {
        headers:
          {
          }
      }).then((result) => {
      commit('updateWifis', result.data)
    }).catch(function (error) {
      console.log(error)
    })
  },
  async connect ({ dispatch, rootGetters }, wifi) {
    return new Promise(async (resolve, reject) => {
      axios.post(`${process.env.VUE_APP_API_ENDPOINT}/api/v1/wifis/`, wifi)
        .then((result) => {
          resolve(result)
        }).catch((error) => {
          console.log(error)
          reject(error)
        })
    })
  }
}

const mutations = {
  updateWifis (state, wifis) {
    state.wifis = wifis
  },
  updateCurrentWifi (state, value) {
    state.currentWifi = value
  },
  updatePassword (state, pw) {
    state.currentWifi.password = pw
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
