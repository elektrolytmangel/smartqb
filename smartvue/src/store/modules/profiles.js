import axios from 'axios'

const state = {
  profiles: [],
  currentProfile: { name: '' },
  profileTimer: null
}

const getters = {
  currProfName (state) {
    if (state.currentProfile == null) {
      return ''
    }
    return state.currentProfile.name
  },
  profiles (state) {
    return state.profiles
  },
  alarmTime (state) {
    if (state.currentProfile == null) {
      return ''
    }
    return state.currentProfile.alarm_clock_time
  },
  alarmClockActivated (state) {
    if (state.currentProfile == null) {
      return false
    }
    return state.currentProfile.alarm_clock_activated
  }
}

const actions = {
  get ({ commit, dispatch }) {
    return new Promise((resolve) => {
      axios.get(`${process.env.VUE_APP_API_ENDPOINT}/api/v1/profiles/`).then((result) => {
        commit('updateProfiles', result.data)
        for (var pro of result.data) {
          if (pro.default === true) {
            dispatch('setCurrProf', pro)
          }
        }
        if (result.data.length > 0) {
          resolve(true)
        } else {
          resolve(false)
        }
      }).catch(function (error) {
        console.log(error)
      })
    })
  },
  createProfile ({ dispatch, commit, state, rootGetters }) {
    var pro = state.currentProfile
    var location = rootGetters['l/currentLocation']
    pro.latitude = location.lat
    pro.longitude = location.lng
    dispatch('collectProfileInfo', pro).then((resultProfile) => {
      axios.post(`${process.env.VUE_APP_API_ENDPOINT}/api/v1/profiles/`, resultProfile)
        .then((result) => {
          commit('addProfile', result.data)
        })
        .catch(function (error) {
          console.log(error)
        })
    })
  },
  setCurrProf ({ dispatch, commit, state }, profile) {
    dispatch('collectProfileInfo', profile).then((result) => {
      console.log('successfull updated profile-infos')
      if (profile.name !== state.currentProfile.name) {
        axios.put(`${process.env.VUE_APP_API_ENDPOINT}${result.url}`, result).then((resultProfile) => {
          commit('udtCurrProf', resultProfile.data)
        }).catch((error) => {
          console.log(error)
        })
      }   
    })
  },
  clearCurrProf ({ commit }) {
    commit('udtCurrProf', null)
  },
  async collectProfileInfo ({ dispatch, rootGetters }, udtPro) {
    return new Promise(async (resolve, reject) => {
      var loc = {
        lat: udtPro.latitude,
        lng: udtPro.longitude
      }
      await dispatch('l/getLocationInfo', loc, { root: true })
      var curLoc = rootGetters['l/currentLocation']
      udtPro.default = true
      udtPro.latitude = loc.lat
      udtPro.longitude = loc.lng
      udtPro.location = curLoc.city
      udtPro.time_zone = curLoc.timezone
      resolve(udtPro)
    })
  },
  setCurrProfName ({ commit }, name) {
    commit('udtcurrProfName', name)
  },
  updateProfileLocation ({ dispatch, state, rootGetters }) {
    var curLoc = rootGetters['l/currentLocation']
    var pro = state.currentProfile
    pro.latitude = curLoc.lat
    pro.longitude = curLoc.lng
    dispatch('setCurrProf', pro)
  }
}

const mutations = {
  addProfile (state, profile) {
    state.profiles.push(profile)
  },
  updateProfiles (state, profiles) {
    state.profiles = profiles
  },
  udtcurrProfName (state, name) {
    if (state.currentProfile == null) {
      state.currentProfile = {
        name: ''
      }
    }
    state.currentProfile.name = name
  },
  udtCurrProf (state, profile) {
    state.currentProfile = profile
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
