import Vue from 'vue'
import Vuex from 'vuex'
import profiles from './modules/profiles'
import wifis from './modules/wifis'
import locations from './modules/locations'
import translation from './modules/translation'
import pixels from './modules/pixels'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    p: profiles,
    w: wifis,
    l: locations,
    t: translation,
    e: pixels
  }
})
