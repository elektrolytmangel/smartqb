import Vue from 'vue'
import vueBulmaComponents from 'vue-bulma-components'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import Leaflet from 'leaflet'
import moment from 'moment'
import VueMomentJS from 'vue-momentjs'
import router from './router'
import store from './store'
import App from './App.vue'

library.add(fas)

Vue.config.productionTip = false
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.use(vueBulmaComponents)
Vue.use(Leaflet)
Vue.use(VueMomentJS, moment)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
