from flask import Blueprint

controller = Blueprint('controller', __name__)

# import needed classes - modules
from .profileController import ProfileController
from .pixelController import PixelController
from .wifiController import WifiController
from .serialController import SerialController
