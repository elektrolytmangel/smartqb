# -*- coding: utf-8 -*-

import wifi
from wifi import Scheme
import subprocess


class PiScheme(Scheme):

    def __init__(self, interface, name, options=None):
        super().__init__(interface, name, options)

    def activate(self):
        """
        Connects to the network as configured in this scheme. Override for pi-wlan commands without options.
        """
        subprocess.check_output(['sudo', 'ifconfig', self.interface, 'down'], stderr=subprocess.STDOUT)
        ifup_output = subprocess.check_output(['sudo', 'ifconfig', self.interface, 'up'], stderr=subprocess.STDOUT)
        ifup_output = ifup_output.decode('utf-8')


class WifiController(object):

    @staticmethod
    def search():
        wifilist = []

        cells = wifi.Cell.all('wlan0')

        for cell in cells:
            wifilist.append(cell)

        return wifilist

    def find_from_search_list(self, ssid):
        wifilist = self.search()

        for cell in wifilist:
            if cell.ssid == ssid:
                return cell

        return False

    @staticmethod
    def find_from_saved_list(ssid):
        cell = PiScheme.find('wlan0', ssid)

        if cell:
            return cell

        return False

    def connect(self, ssid, password=None):
        cell = self.find_from_search_list(ssid)

        if cell:
            savedcell = self.find_from_saved_list(cell.ssid)

            # Already Saved from Setting
            if savedcell:
                savedcell.activate()
                return cell

            # First time to conenct
            else:
                if cell.encrypted:
                    if password:
                        scheme = self.add(cell, password)

                        try:
                            scheme.activate()

                        # Wrong Password
                        except wifi.exceptions.ConnectionError:
                            self.delete(ssid)
                            return False

                        return cell
                    else:
                        return False
                else:
                    scheme = self.add(cell)

                    try:
                        scheme.activate()
                    except wifi.exceptions.ConnectionError:
                        self.delete(ssid)
                        return False

                    return cell

        return False

    @staticmethod
    def add(cell, password=None):
        if not cell:
            return False

        scheme = PiScheme.for_cell('wlan0', cell.ssid, cell, password)
        scheme.save()
        return scheme

    def delete(self, ssid):
        if not ssid:
            return False

        cell = self.find_from_saved_list(ssid)

        if cell:
            cell.delete()
            return True

        return False

    @staticmethod
    def to_json(wlan):
        json_wlan = {
            'ssid': wlan.ssid,
            'encrypted': wlan.encrypted,
            'signal': wlan.signal,
            'quality': wlan.quality,
            'password': ''
        }
        return json_wlan
