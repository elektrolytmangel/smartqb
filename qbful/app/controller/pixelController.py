import datetime
import time
import logging
import os
from ..models import Weather, SensorData
from .serialController import SerialController
from apscheduler.schedulers.background import BackgroundScheduler
from ..lib.opc import Client

# important: one of the led-elements will be on the second index of the fadecandy,
# for this case ther are the start_idx-variables
# the data are not persistent, so if there is a mulithreaded wsgi-server it will throw some exceptions from time to time


class PixelController(object):
    __instance = None
    LED_MODES = {0: 'off', 1: 'day_night', 2: 'time', 3: 'weather', 4: 'alarm_clock'}
    NUM_LED_OUTER = int(os.getenv('NUM_LED_OUTER')) or 60
    NUM_LED_INNER = int(os.getenv('NUM_LED_INNER')) or 48
    START_IDX_OUTER = int(os.getenv('START_IDX_OUTER')) or 0  # first ring connected to fadecandy
    START_IDX_INNER = int(os.getenv('START_IDX_INNER')) or 60  # second ring connected to fadecandy
    OFFSET_TO_ZERO_OUTER = int(os.getenv('LED_OFFSET_OUT')) or 0  # setting up which led is on buttom counting in clockwise direction from the effectiv zero pixel to the pixel you want to be zero
    OFFSET_TO_ZERO_INNER = int(os.getenv('LED_OFFSET_IN')) or 0  # setting up which led is on buttom counting in clockwise direction from the effectiv zero pixel to the pixel you want to be zero
    SIX_HOURS = 360
    MAX_CLOUDS = 100
    MOTION_DELAY = int(os.getenv('MOTION_DELAY')) or 10 # seconds to wait after last status change for motion activation

    def __new__(cls):
        if PixelController.__instance is None:
            PixelController.__instance = object.__new__(cls)
        return PixelController.__instance

    def __init__(self):
        self.logger = logging.getLogger('flask.app')
        self.scheduler = BackgroundScheduler()
        self.interval_time = 5
        self.interval_alarm = 4
        self.job = None
        self.current_mode = None
        self.default_mode = 1  # default shows time
        self.last_modus_change = datetime.datetime.now() # initialize current date
        self.last_run = None
        self.scheduler.start()  # start scheduler, even if currently no job is available
        self.brightness = 0.7  # default value
        self.min_brightness = float(os.getenv('MIN_BRIGHTNESS')) or 0.3
        self.max_rain = float(os.getenv('MAX_RAIN')) or 10.0  # sets max rain in mm
        self.motion_registred = False  # default value
        self.colors = self.color_collection(self.brightness)
        self.total_num_pixels = self.NUM_LED_OUTER + self.NUM_LED_INNER
        self.pixels = [(0, 0, 0)] * self.total_num_pixels  # create list with all leds available set to black / off
        self.client = Client(os.getenv('FADECANDY_URL') or 'localhost:7890')
        # (time is depending on 12hour-frequenzies, one hour has 60 minutes
        # = total minutes which can be lighted up) / total leds available
        # = how many minutes fill up one led
        self.minutes_per_pix_outer = 12 * 60 / self.NUM_LED_OUTER
        self.minutes_per_pix_inner = 12 * 60 / self.NUM_LED_INNER
        # start listening on serial port
        self.serial_listener = SerialController(os.getenv('ARDUINO_USB') or 'ttyUSB0', 9600)
        # get notificated if sensor data changes, this is needed for the motion sensor and also for ldr
        self.sensor_listener = SensorData()
        self.sensor_listener.bind_to(self.handle_sensor_data)

    @staticmethod
    def datetime_to_daymiutes(dt):
        hour = dt.hour
        minute = dt.minute
        daytime_in_minutes = (hour * 60) + minute
        return daytime_in_minutes

    def _day_night_mode(self, now):
        self.logger.info("start day-night-mode")
        # resetting pixels here
        self.set_all_pixels(self.colors['black'])
        self.set_quarter_time_markings(self.NUM_LED_INNER, self.START_IDX_INNER + self.OFFSET_TO_ZERO_INNER, self.colors['lightblue'], self.START_IDX_INNER, self.START_IDX_INNER + self.NUM_LED_INNER - 1, 2)
        dayminutes = self.datetime_to_daymiutes(now)
        self.day_night_mode_outer(dayminutes)
        sunset = self.datetime_to_daymiutes(Weather.sunset)
        sunrise = self.datetime_to_daymiutes(Weather.sunrise)
        self.day_night_mode_inner([sunset, sunrise])
        self.logger.debug(self.pixels)
        self.put_pixels()
    
    def day_night_mode(self):
        now = datetime.datetime.now()
        self._day_night_mode(now)

    def day_night_mode_outer(self, daytime_in_minutes):
        """
        get leds to light up in the outer-ring
        """
        self.logger.debug("Daytime = {}".format(daytime_in_minutes))
        # find out: how many pixel to light up, starting by index and color (and brightness) depending on the current time
        six_hour_idx = self.get_dn_six_hour_index(daytime_in_minutes)
        color = self.get_dn_color(six_hour_idx)
        start_index = 0
        self.logger.debug("Six-Hour-Idx = {}".format(six_hour_idx))
        pixels_to_light_up = int((daytime_in_minutes - (self.SIX_HOURS * six_hour_idx)) / self.minutes_per_pix_outer)
        if six_hour_idx in [0, 2]:
            # first light up all pixels, then turn of one pixel after another every x-minutes
            start_index = self.START_IDX_OUTER + self.OFFSET_TO_ZERO_OUTER + int((self.NUM_LED_OUTER / 2))  # setting start index to top
            start_index += pixels_to_light_up
            pixels_to_light_up = int((self.NUM_LED_OUTER / 2) - pixels_to_light_up) + 1
        elif six_hour_idx in [1, 3]:
            pixels_to_light_up += 1
            start_index =  self.START_IDX_OUTER + self.OFFSET_TO_ZERO_OUTER  # setting start index to bottom
        self.logger.debug("Params outer: start_index={}, pixels_count={}, min={}, max={}".format(start_index, pixels_to_light_up, self.START_IDX_OUTER, self.START_IDX_OUTER + self.NUM_LED_OUTER - 1))
        self.light_up_pixel_range(start_index, pixels_to_light_up, color, self.START_IDX_OUTER, self.START_IDX_OUTER + self.NUM_LED_OUTER - 1)

    def day_night_mode_inner(self, sunrise_sunset):
        count = 0
        for daytime in sunrise_sunset:
            if count < 2:
                six_hour_idx = self.get_dn_six_hour_index(daytime)
                color = self.get_dn_color(six_hour_idx)
                pix = int(daytime / self.minutes_per_pix_inner) + self.START_IDX_INNER
                max_idx_inner = self.START_IDX_INNER + self.NUM_LED_INNER - 1
                self.light_up_pixel_range(pix, 1, color, self.START_IDX_INNER, max_idx_inner)  # light up just one led, but use the available logic to overclock
            else:
                break
            count += 1

    def get_dn_color(self, six_hour_index):
        """
        Get day-night color-schema based on current time
        :param six_hour_index: current time in minutes
        :retrun: RGB color-value
        """
        # between midnight and 6am
        if six_hour_index == 0:
            return self.colors['white']
        # between 6am and noon
        elif six_hour_index == 1:
            return self.colors['darkorange']
        # between noon and 6pm
        elif six_hour_index == 2:
            return self.colors['darkorange']
        # between 6pm and midnight
        elif six_hour_index == 3:
            return self.colors['white']
        else:
            return self.colors['black']

    def get_dn_six_hour_index(self, daytime_in_minutes):
        """
        Gets index of current time-quarter starting at 00:00 AM
        :param daytime_in_minutes: current time in minutes
        :return: (int) index based on 0. Available values 0, 1, 2, 3
        """
        # between midnight and 6am
        if daytime_in_minutes in range(0, self.SIX_HOURS):
            return 0
        # between 6am and noon
        elif daytime_in_minutes in range(self.SIX_HOURS, self.SIX_HOURS * 2):
            return 1
        # between noon and 6pm
        elif daytime_in_minutes in range(self.SIX_HOURS * 2, self.SIX_HOURS * 3):
            return 2
        # between 6pm and midnight
        elif daytime_in_minutes in range(self.SIX_HOURS * 3, self.SIX_HOURS * 4):
            return 3

    def light_up_pixel_range(self, start_idx, count, color, min_idx, max_idx):
        """
        Lights up neopixels starting at given index, ending at start-index + count
        :param start_idx: proper index for the first led to light up, offset must be already included
        :param count: how many pixels has to be light up in range starting at start-index
        :param color: color to light up the pixels with
        :param min_idx: minimal index which can be lighted up, i.e. min index of the pixel-ring relating to the fadecandy for the first ring it will be 0
        :param max_idx: same as min_idx just the max
        :return: None
        """
        if start_idx < min_idx:
            raise Exception("start-index is not in given range")
        pixels = self.pixels  # get the already lighted up pixels, so pixels outside the given max and min will not be affected
        substracter = 0
        start_corr = 0
        if start_idx > max_idx:
            start_corr = 1
        num_pix = max_idx - min_idx + 1
        for pix in range(start_idx, start_idx + count):
            if pix > max_idx and start_idx > max_idx:
                substracter = -1
            count_overclock = int((pix - min_idx) / num_pix)  # if the max index is more than once overclocked
            pix -= (num_pix * count_overclock) + substracter + start_corr # going back to min index resp. applie the offset, because count is already higher than the max,
            pixels[pix] = color
        self.pixels = pixels

    def set_quarter_time_markings(self, num_pix, start_idx, color, min_idx, max_idx, width=1, split=4):
        """
        Sets every quarter pixel to a given color. This is used to create the markings at 6am, 9am, 12pm and so on
        :param num_pix: total number of pixel wich you want to divide in quarters i.e. number of pixels from the outer-ring
        :param start_idx: proper index for the first led to light up, offset must be already included
        :param color: color to light up the pixels with
        :param min_idx: minimal index which can be lighted up, i.e. min index of the pixel-ring relating to the fadecandy for the first ring it will be 0
        :param max_idx: same as min_idx just the max
        :param width: how many pixels to add to the start, set this to more than one if you want have a wider pixel, alwasy from start index
        :param split: how many parts to split by
        """
        step = int(num_pix / split)  # get length of one quarter (split-part) of the ring / given range, and make it to an int
        for i in range(start_idx, start_idx + num_pix - 1, step):
            self.light_up_pixel_range(i, width, color, min_idx, max_idx)

    def set_all_pixels(self, color):
        self.pixels = [color] * self.total_num_pixels

    def countdown(self, end_idx, count, color, min_idx, max_idx, duration=3):
        """
        counts down to the end index, turns off one pixel after another
        """
        time_sleep = duration / count
        for i in range(count):
            self.light_up_pixel_range(end_idx, count, self.colors['black'], min_idx, max_idx)
            pixels_to_light_up = count - i
            self.light_up_pixel_range(end_idx, pixels_to_light_up, color, min_idx, max_idx)
            self.set_quarter_time_markings(self.NUM_LED_OUTER, self.START_IDX_OUTER + self.OFFSET_TO_ZERO_OUTER, self.colors['white'], self.START_IDX_OUTER, self.START_IDX_OUTER + self.NUM_LED_OUTER - 1, split=12)
            self.put_pixels()
            time.sleep(time_sleep)

    def time_mode_outer(self, minutes, seconds):
        # find out: how many pixel to light up, starting by index and color (and brightness) depending on the current time
        color = self.colors['gold']
        # start_index = int((self.OFFSET_TO_ZERO_OUTER + self.START_IDX_OUTER)) # ) + (self.NUM_LED_OUTER / 2))  # do not care about to high indexes, it will be catched in light_up_pixel_range
        index_bottom = self.OFFSET_TO_ZERO_OUTER + self.START_IDX_OUTER
        pix_to_add = self.NUM_LED_OUTER / 2
        start_index = int(index_bottom + pix_to_add)
        if minutes < 59 or seconds < (60 - self.interval_time):  # give some space because of the interval from 5 secs
            pixels_to_light_up = int((self.NUM_LED_OUTER / 60 * minutes)) + 1 # always light up one pixel
            self.logger.debug("Params outer: start_index={}, pixels_count={}, min={}, max={}".format(start_index, pixels_to_light_up, self.START_IDX_OUTER, self.START_IDX_OUTER + self.NUM_LED_OUTER - 1))
            self.light_up_pixel_range(start_index, pixels_to_light_up, color, self.START_IDX_OUTER, self.START_IDX_OUTER + self.NUM_LED_OUTER - 1)
        else:
            self.countdown(start_index, self.NUM_LED_OUTER, color, self.START_IDX_OUTER, self.START_IDX_OUTER + self.NUM_LED_OUTER - 1)
    
    def time_mode_inner(self, dayminutes):
        # find out: how many pixel to light up, starting by index and color (and brightness) depending on the current time
        color = self.colors['gold']
        pixels_to_light_up = int((self.NUM_LED_INNER / (12 * 60 ) * dayminutes))
        index_bottom = self.OFFSET_TO_ZERO_INNER + self.START_IDX_INNER
        pix_to_add = self.NUM_LED_INNER / 2
        start_index = int(index_bottom + pix_to_add + pixels_to_light_up)
        self.logger.debug("Params inner: start_index={}, pixel_at={}, min={}, max={}".format(start_index, pixels_to_light_up, self.START_IDX_INNER, self.START_IDX_INNER + self.NUM_LED_INNER - 1))
        self.light_up_pixel_range(start_index, 2, color, self.START_IDX_INNER, self.START_IDX_INNER + self.NUM_LED_INNER - 1)

    def _time_mode(self, now):
        self.logger.info("start time-mode " + str(now))
        self.set_all_pixels(self.colors['black'])
        # self.set_quarter_time_markings(self.NUM_LED_INNER, self.START_IDX_INNER + self.OFFSET_TO_ZERO_INNER, self.colors['white'], self.START_IDX_INNER, self.START_IDX_INNER + self.NUM_LED_INNER - 1, 2)
        dayminutes = self.datetime_to_daymiutes(now)
        self.time_mode_inner(dayminutes)
        self.time_mode_outer(now.minute, now.second)
        self.set_quarter_time_markings(self.NUM_LED_OUTER, self.START_IDX_OUTER + self.OFFSET_TO_ZERO_OUTER, self.colors['white'], self.START_IDX_OUTER, self.START_IDX_OUTER + self.NUM_LED_OUTER -1, split=12)
        if now.minute % 5 == 0:
            idx = self.OFFSET_TO_ZERO_OUTER + (self.NUM_LED_OUTER / 2)
            pix = self.NUM_LED_OUTER / 60 * now.minute
            override_pix_idx = int(idx + pix)
            self.light_up_pixel_range(override_pix_idx, 1, self.colors['gold'], self.START_IDX_OUTER, self.START_IDX_OUTER + self.NUM_LED_OUTER - 1)
        self.put_pixels()

    def time_mode(self):
        """
        Override to call from interval
        """
        now = datetime.datetime.now()
        self._time_mode(now)
    
    def get_temp_color(self, temp):
        """
        Get day-night color-schema based on current time
        :param temp: current temperature
        :return: RGB color-value
        """
        if temp < -10:
            return self.colors['blue']
        elif temp < 0:
            return self.colors['lightblue']
        elif temp < 10:
            return self.colors['white']
        elif temp < 20:
            return self.colors['green']
        elif temp < 30:
            return self.colors['yellow']
        elif temp < 40:
            return self.colors['orange']
        else:
            return self.colors['red']

    @staticmethod
    def get_start_idx_weather(total_pix, num_pix_ring, start_idx_ring):
        """
        gets the start index for weather mode, it will always be different, so calculate it here
        :param total_pix: total number of pixel to light up
        :param num_pix_ring: number of pixel available on the ring (inner / outer)
        :param start_idx_ring: proper index of the first led which can be lighted up (zero index of the ring), offset must be already included
        :return absolute starting index:
        """
        sub = int(total_pix / 2)
        if start_idx_ring - sub >= 0:
            return start_idx_ring - sub
        else:
            return num_pix_ring + start_idx_ring - sub

    def weather_mode_outer(self, temp, clouds):
        self.logger.debug("Cloudyness={}, Temperature={}".format(Weather.clouds, Weather.temp))
        # find out: how many pixel to light up, starting by index and color (and brightness) depending on the current time
        color = self.get_temp_color(temp)
        pixels_to_light_up = int((self.NUM_LED_OUTER / self.MAX_CLOUDS) * (self.MAX_CLOUDS - clouds))
        if pixels_to_light_up < 5:
            pixels_to_light_up = 5
        if pixels_to_light_up % 2 == 0:
            pixels_to_light_up += 1
        start_index = self.get_start_idx_weather(pixels_to_light_up, self.NUM_LED_OUTER, self.OFFSET_TO_ZERO_OUTER + self.START_IDX_OUTER)
        self.logger.debug("Params outer: start_index={}, pixels_count={}, min={}, max={}".format(start_index, pixels_to_light_up, self.START_IDX_OUTER, self.START_IDX_OUTER + self.NUM_LED_OUTER - 1))
        self.light_up_pixel_range(start_index, pixels_to_light_up, color, self.START_IDX_OUTER, self.START_IDX_OUTER + self.NUM_LED_OUTER - 1)

    def weather_mode_inner(self, rain):
        self.logger.debug("Rainyness={}".format(Weather.rain))
        # find out: how many pixel to light up, starting by index and color (and brightness) depending on the current time
        color = self.colors['lightblue']
        pixels_to_light_up = int((self.NUM_LED_INNER / self.max_rain) * rain)
        if pixels_to_light_up % 2 != 0:
            pixels_to_light_up += 1
        start_index = self.get_start_idx_weather(pixels_to_light_up, self.NUM_LED_INNER, self.OFFSET_TO_ZERO_INNER + self.START_IDX_INNER)
        self.logger.debug("Params inner: start_index={}, pixels_count={}, min={}, max={}".format(start_index, pixels_to_light_up, self.START_IDX_INNER, self.START_IDX_INNER + self.NUM_LED_INNER - 1))
        self.light_up_pixel_range(start_index, pixels_to_light_up, color, self.START_IDX_INNER, self.START_IDX_INNER + self.NUM_LED_INNER - 1)

    def _weather_mode(self, rain, temp, clouds):
        self.logger.info("start weather-mode")
        self.logger.debug("Current weather data: sunset={}, sunrise={}, temp={}, rain={}, clouds={}".format(Weather.sunset, Weather.sunrise, Weather.temp, Weather.rain, Weather.clouds))
        self.set_all_pixels(self.colors['black'])
        self.weather_mode_outer(temp, clouds)
        self.weather_mode_inner(rain)
        self.put_pixels()

    def weather_mode(self):
        rain = Weather.rain
        temp = Weather.temp
        clouds = Weather.clouds
        self._weather_mode(rain, temp, clouds)

    def alarm_mode(self):
        self.logger.info("start alarm-mode")
        self.set_all_pixels(self.colors['tomato'])
        self.put_pixels()
        time.sleep(self.interval_alarm / 2)
        self.set_all_pixels(self.colors['black'])
        self.put_pixels()

    def test_1(self):
        for h in range(24):
            for m in range(59):
                time_now = datetime.time(h, m)
                self._day_night_mode(time_now)
                time.sleep(0.1)
        self.set_all_pixels(self.colors['black'])
        self.put_pixels()
        time.sleep(1)

    def test_2(self):
        self.set_all_pixels(self.colors['black'])
        for xhour in range(5):
            for yminute in range(59):
                time_now = datetime.time(xhour, yminute, 59)
                self._time_mode(time_now)
                time.sleep(0.3)
        self.set_all_pixels(self.colors['black'])
        self.put_pixels()
        time.sleep(1)

    def test_3(self):
        for s in range(0, 100, 10):
            self._weather_mode(2, 25, s)
            time.sleep(0.3)
        for r in range(10):
            self._weather_mode(r, 9, 90)
            time.sleep(1)
        self.set_all_pixels(self.colors['black'])
        self.put_pixels()
        time.sleep(1)

    def test_mode(self):
        self.logger.info("start test-mode")
        self.set_all_pixels(self.colors['tomato'])
        self.put_pixels()
        time.sleep(5)
        self.test_1()
        self.test_2()
        self.test_3()
        for i in range(5):
            self.alarm_mode()
            time.sleep(self.interval_alarm)

    def reset_job(self):
        if self.job is not None and self.current_mode is not 0:
            self.job.remove()
            self.job = None

    def start_pixel(self, mode):
        """
        starting point. befor creating the apscheduler-job, call the target method direct. if not it will be very slow
        """
        self.last_modus_change = datetime.datetime.now()
        self.set_all_pixels(self.colors['black']) # set all pixel to zero if mode changes
        message = "led-mode = {0} not available".format(mode) 
        if mode == 0:
            # just stopping the leds
            self.reset_job()
            self.set_all_pixels(self.colors['black']) # set all pixel to zero if mode changes
            self.put_pixels()
            message = "success"
        elif mode == 1:
            self.time_mode()
            self.reset_job()
            self.job = self.scheduler.add_job(self.time_mode, 'interval', seconds=self.interval_time)
            self.default_mode = mode # change default mode
            message = "success"
        elif mode == 2:
            self.day_night_mode()
            self.reset_job()
            self.job = self.scheduler.add_job(self.day_night_mode, 'interval', seconds=self.interval_time)
            self.default_mode = mode # change default mode
            message = "success"
        elif mode == 3:
            self.weather_mode()
            self.reset_job()
            self.job = self.scheduler.add_job(self.weather_mode, 'interval', seconds=self.interval_time)
            self.default_mode = mode # change default mode
            message = "success"
        elif mode == 4:
            self.alarm_mode()
            self.reset_job()
            self.job = self.scheduler.add_job(self.alarm_mode, 'interval', seconds=self.interval_alarm)
            message = "success"
        elif mode == 99:
            # test-mode
            self.reset_job()
            self.job = self.scheduler.add_job(self.test_mode, 'date')
            message = "success"
        self.current_mode = mode
        return message

    def put_pixels(self):
        if self.client.put_pixels(self.pixels):
            self.logger.debug("successfull set pixels")
        else:
            self.logger.error("cannot set pixels, because fadecandy is not available")

    def handle_sensor_data(self, data):
        accept_motion_activation = datetime.datetime.now() - self.last_modus_change > datetime.timedelta(0, self.MOTION_DELAY)
        if data.brightness > self.min_brightness:
            self.brightness = data.brightness
            self.colors = self.color_collection(self.brightness)
        if data.motion and self.current_mode is 0 and accept_motion_activation:
            self.start_pixel(self.default_mode)

    @staticmethod
    def color_collection(ldr):
        white = (int(255*ldr), int(255*ldr), int(255*ldr))
        coral = (int(255*ldr), int(127*ldr), int(80*ldr))
        tomato = (int(255*ldr), int(99*ldr), int(71*ldr))
        orangered = (int(255*ldr), int(69*ldr), int(0*ldr))
        gold = (int(255*ldr), int(215*ldr), int(0*ldr))
        orange = (int(255*ldr), int(165*ldr), int(0*ldr))
        darkorange = (int(255*ldr), int(140*ldr), int(0*ldr))
        lightblue = (int(124*ldr), int(185*ldr), int(232*ldr))
        blue = (int(10*ldr), int(10*ldr), int(255*ldr))
        yellow = (int(255*ldr), int(255*ldr), int(0*ldr))
        green = (int(10*ldr), int(255*ldr), int(10*ldr))
        red = (int(255*ldr), int(10*ldr), int(10*ldr))
        black = (int(0*ldr), int(0*ldr), int(0*ldr))
        return {'white': white, 'blue': blue, 'lightblue': lightblue, 'red': red, 
                'yellow': yellow, 'green': green, 'coral': coral, 'tomato': tomato, 
                'orangered': orangered, 'gold': gold, 'orange': orange, 'darkorange': darkorange, 
                'black': black}
