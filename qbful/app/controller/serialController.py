import serial
import serial.threaded
import logging
import json
from apscheduler.schedulers.background import BackgroundScheduler
from ..models import SensorData


class SerialToPix(serial.threaded.LineReader):
    """gets the data from serial port and send them to the pixelcontroller"""

    def __init__(self):
        super().__init__()
        self.logger = logging.getLogger('flask.app')
        self.sensorData = SensorData()

    def __call__(self):
        return self

    def handle_line(self, packet):
        try:
            self.logger.debug("got sensor data: {}".format(packet))
            jsondata = json.loads(packet)
            if jsondata is not "":
                pir = jsondata["pir"]
                ldr = jsondata["ldr"]
                if pir == 1:
                    self.sensorData.motion = True
                else:
                    self.sensorData.motion = False
                self.sensorData.brightness = ldr / 1000
        except Exception as e:
            print(e)
            self.logger.error(e)


class SerialController(object):

    def __init__(self, port, baud):
        self.logger = logging.getLogger('flask.app')
        self.port = port
        self.baud = baud
        self.scheduler = BackgroundScheduler()
        self.scheduler.start()  # start scheduler, even if currently no job is available
        self.restart_job = None
        # start listening
        self.serial_instance = self.connect()
        self.worker = self.listen(0)

    def connect(self):
        serial_instance = serial.Serial()
        try:
            serial_instance = serial.Serial(port=self.port, baudrate=self.baud)
            if not serial_instance.is_open:
                serial_instance.open()
            self.logger.info('successful opened serial port {}'.format(self.port))
        except serial.SerialException as e:
            self.logger.error('Could not open serial port {}: {}'.format(self.port, e))
            if self.restart_job is None:
                self.restart_job = self.scheduler.add_job(self.restart, 'interval', seconds=30)
        # always return an instance, so later can be checked if the port is open
        return serial_instance

    def listen(self, count):
        if self.serial_instance.is_open:
            ser_to_pix = SerialToPix()
            serial_worker = serial.threaded.ReaderThread(self.serial_instance, ser_to_pix)
            serial_worker.start()
            return serial_worker
        elif count < 3:
            self.logger.warning('serial port {} is not open. try again to open it...'.format(self.port))
            self.connect()
            self.worker = self.listen(count + 1)
        else:
            self.logger.error('serial port {} is not open or cannot be found. Stop trying to open it. Please check the configuration'.format(self.port))

    def restart(self):
        self.serial_instance = self.connect()
        if self.serial_instance.is_open:
            self.worker = self.listen(0)
            self.scheduler.remove_all_jobs()
