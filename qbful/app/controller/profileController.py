from flask import current_app
from ..models import Profile
from app import db  # SQLAlchemy
import os
import time


class ProfileController(object):
    @staticmethod
    def get_profile_by_id(id):
        return Profile.query.get_or_404(id)

    @staticmethod
    def get_profiles():
        return Profile.query.all()

    @staticmethod
    def add_profile(profile):
        ProfileController.reset_default_profile(profile)
        profile.default = True
        try:
            db.session.add(profile)
            db.session.commit()
            ProfileController.set_timezone(profile.time_zone)
            return "success"
        except Exception as e:
            return e.args[0]

    @staticmethod
    def udt_profile(profile):
        ProfileController.reset_default_profile(profile)
        profile.default = True
        try:
            db.session.add(profile)
            db.session.commit()
            ProfileController.set_timezone(profile.time_zone)
            return "success"
        except Exception as e:
            return e.args[0]

    @staticmethod
    def reset_default_profile(profile):
        default_profiles = db.session.query(Profile).filter(Profile.default is True, Profile.id != profile.id).all()
        for prof in default_profiles:
            prof.default = False
            db.session.add(prof)
        db.session.commit()

    @staticmethod
    def set_timezone(timezone):
        time_zone_cmd = "sudo timedatectl set-timezone {0}".format(timezone)
        try:
            os.system(time_zone_cmd)
        except Exception as e:
            current_app.logger.exception("Cannot change system-timezone: " + str(e))
        try:
            os.environ['TZ'] = timezone
            time.tzset()
        except Exception as e:
            current_app.logger.exception("Cannot change environment-timezone: " + str(e))