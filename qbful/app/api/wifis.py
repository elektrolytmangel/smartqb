import logging
from flask import jsonify, request
from . import api
from .errors import bad_request
from ..controller import WifiController

wifiController = WifiController()
logger = logging.getLogger('flask.app')


@api.route('/wifis/')
def get_wifis():
    try:
        wifis = wifiController.search()
    except Exception as e:
        return bad_request("failed to get wifis: " + str(e))
    json_list = []
    for w in wifis:
        json_list.append(wifiController.to_json(w))
    return jsonify(json_list)


@api.route('/wifis/', methods=['POST'])
def connect_wifi():
    print(request.json)
    ssid = request.json.get('ssid')
    pwd = request.json.get('password')
    if pwd is not None or pwd is not "":
        try:
            result = wifiController.connect(ssid, pwd)
        except Exception as e:
            logger.error(e)
            result = "failed"
    else:
        try:
            result = wifiController.connect(ssid)
        except Exception as e:
            logger.error(e)
            result = "failed"
    if result == "success":
        return jsonify(result), 201
    else:
        return bad_request(result)
