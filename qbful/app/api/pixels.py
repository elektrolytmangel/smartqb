from flask import jsonify
from . import api
from .errors import bad_request
from ..controller import PixelController

# not pretty, and do not work properly with miltithreaded applications like gunicorn
pixel_controller = PixelController()


@api.route('/pixels/')
def get_pixels():
    current_mode = pixel_controller.current_mode
    pixels = { 'current_mode': current_mode}
    return jsonify(pixels)


@api.route('/pixels/<int:mode>', methods=['PUT'])
def change_pixel(mode):
    result = pixel_controller.start_pixel(mode)
    if result == "success":
        return jsonify(result)
    else:
        return bad_request(result)
