from flask import jsonify, request, url_for
from . import api
from .errors import bad_request
from ..controller import ProfileController
from ..models import Profile


@api.route('/profiles/<int:id>')
def get_profile(id):
    profile = ProfileController.get_profile_by_id(id)
    return jsonify(profile.to_json())


@api.route('/profiles/')
def get_profiles():
    profiles = ProfileController.get_profiles()
    json_list = []
    for prof in profiles:
        json_list.append(prof.to_json())
    return jsonify(json_list)


@api.route('/profiles/', methods=['POST'])
def new_profile():
    profile = Profile()
    profile.from_json(request.json)
    result = ProfileController.add_profile(profile)
    if result == "success":
        return jsonify(profile.to_json()), 201, \
            {'Location': url_for('api.get_profile', id=profile.id)}
    else:
        return bad_request(result)


@api.route('/profiles/<int:id>', methods=['PUT'])
def edit_profile(id):
    profile = Profile.query.get_or_404(id)
    profile.from_json(request.json)
    result = ProfileController.udt_profile(profile)
    if result == "success":
        return jsonify(profile.to_json())
    else:
        return bad_request(result)
