from flask import jsonify, request
from . import api
from ..models import Weather


@api.route('/weather/', methods=['POST'])
def post_weather():
    Weather.from_json(request.json)
    return jsonify("success"), 201
