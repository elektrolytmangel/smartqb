from flask import Blueprint

api = Blueprint('api', __name__)

# import needed api - modules
from . import profiles, pixels, wifis, weather, errors
