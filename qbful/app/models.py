from datetime import datetime
from flask import url_for
from app.exceptions import ValidationError
from . import db


class Profile(db.Model):
    __tablename__ = 'profile'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    default = db.Column(db.Boolean, default=False, index=True)
    time_zone = db.Column(db.String(32))
    location = db.Column(db.String(64))
    latitude = db.Column(db.Float())
    longitude = db.Column(db.Float())
    ssid = db.Column(db.String(64))
    ssid_pwd = db.Column(db.String(64))
    ssid_encryption = db.Column(db.String(10))
    alarm_clock_time = db.Column(db.String(5))
    alarm_clock_activated = db.Column(db.Boolean())
    uptime_on = db.Column(db.String(5))
    uptime_off = db.Column(db.String(5))
    uptime_activated = db.Column(db.Boolean())

    def to_json(self):
        json_profile = {
            'url': url_for('api.get_profile', id=self.id),
            'name': self.name,
            'default': self.default,
            'time_zone': self.time_zone,
            'location': self.location,
            'latitude': self.latitude,
            'longitude': self.longitude,
            'ssid': self.ssid,
            'ssid_pwd': self.ssid_pwd,
            'ssid_encryption': self.ssid_encryption,
            'alarm_clock_time': self.alarm_clock_time,
            'alarm_clock_activated': self.alarm_clock_activated,
            'uptime_on': self.uptime_on,
            'uptime_off': self.uptime_off,
            'uptime_activated': self.uptime_activated
        }
        return json_profile

    def from_json(self, json_profile):
        self.name = json_profile.get('name')
        self.latitude = json_profile.get('latitude')
        self.longitude = json_profile.get('longitude')
        self.location = json_profile.get('location')
        self.time_zone = json_profile.get('time_zone')
        self.ssid = json_profile.get('ssid')
        self.ssid_pwd = json_profile.get('ssid_pwd')
        self.ssid_encryption = json_profile.get('ssid_encryption')
        self.alarm_clock_time = json_profile.get('alarm_clock_time')
        self.alarm_clock_activated = json_profile.get('alarm_clock_activated')
        self.uptime_on = json_profile.get('uptime_on')
        self.uptime_off = json_profile.get('uptime_off')
        self.uptime_activated = json_profile.get('uptime_activated')
        if self.name is None or self.name == '':
            raise ValidationError('profile does not have a name')


class Weather(object):
    """
    This class is used to cache the weather-data received from the openweatherapi / smartvue-frontend.
    All attributes are on class level and there is no database presistance, because the data are valid for no longer than a few minutes.
    """
    sunset = datetime.fromtimestamp(1566016196)  # Datetime
    sunrise = datetime.fromtimestamp(1566067226)  # Datetime
    clouds = 0  # int max 100
    temp = 0  # int
    rain = 0.0  # float

    @staticmethod
    def from_json(json):
        Weather.sunset = datetime.fromtimestamp(json.get('sunset'))
        Weather.sunrise = datetime.fromtimestamp(json.get('sunrise'))
        Weather.clouds = int(float(json.get('clouds')))
        Weather.temp = int(float(json.get('temp')))
        Weather.rain = float(json.get('rain'))


class SensorData(object):
    """
    This class is used to cache the sensor-data received from the arduino.
    All attributes are on class level and there is no database presistance, because the data are valid for no longer than a few seconds.
    """
    _brightness = 0.7  # float between 0 and 1
    _motion = False  # boolean
    _observers = []

    @property
    def brightness(self):
        return self._brightness

    @brightness.setter
    def brightness(self, value):
        if 1 > value > 0:
            self._brightness = value
            self.notify()

    @property
    def motion(self):
        return self._motion

    @motion.setter
    def motion(self, value):
        if value != self._motion:
            self._motion = value
        if self.motion:
            self.notify()

    def notify(self):
        for callback in self._observers:
            callback(self)

    def bind_to(self, callback):
        self._observers.append(callback)
